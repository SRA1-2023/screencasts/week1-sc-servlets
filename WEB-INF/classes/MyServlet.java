import javax.servlet.http.*;
import javax.servlet.ServletException;

import java.io.IOException;

import javax.servlet.annotation.WebServlet;

// Add a WebServlet annotation to map this class to an URL location
public class MyServlet 
// this class must extends HttpServlet to really become one ..
// extends ...
{

  // override service method, called by servlet container (tomcat) when
  // it match URL location pattern
	public void service(HttpServletRequest req, HttpServletResponse res) 
	throws ServletException
	{
		try {
			// get a printer from response and write something !
			// it would be sent to the user browser.
		} catch( IOException ioe ) {
			throw new ServletException( ioe );
		}

	}

}
